//
//  Alerts.swift
//  MilCrypt
//
//  Created by Denys Borysiuk on 06.07.2018.
//  Copyright © 2018 ArchiSec Solutions, Ltd. All rights reserved.
//

import UIKit

typealias CompletionBlock = () -> Void
typealias CompletionTextBlock = (String?) -> Void

struct AlertSelection {
    let name:String
    let handler:CompletionBlock
}

class Alert: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var messageView: UITextView!
    
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!

    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    @IBOutlet weak var messageHeight: NSLayoutConstraint!
    @IBOutlet weak var thitdButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var underlineWidth: NSLayoutConstraint!
    
    enum AlertType {
        case oneChoice
        case twoChoices
        case threeChoices
    }
    
    private var alertType:AlertType = .oneChoice
    private var message:String = ""
    private var actions:[String] = []
    
    private var firstHandler:CompletionBlock?
    private var secondHandler:CompletionBlock?
    private var thirdHandler:CompletionBlock?
    private var cancelHandler:CompletionBlock?

    private var confirmTitle = "Confirm"
    private var discardTitle = "Discard"
    
    private var presenter:UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        titleView.text = self.title
        containerView.setupBorder(UIColor.clear, radius: 5)
        underlineWidth.constant = self.title!.width(withConstrainedHeight: titleView.frame.height, font: titleView.font)
        
        if alertType == .oneChoice {
            messageView.text = self.message
            firstButton.setTitle("Ok", for: .normal)
            firstButton.setupBorder(UIColor.okColor(), radius: firstButton.frame.size.height / 2.0)
        } else if alertType == .twoChoices {
            messageView.text = self.message
            firstButton.setTitle(confirmTitle, for: .normal)
            firstButton.setupBorder(UIColor.okColor(), radius: firstButton.frame.size.height / 2.0)
            secondButton.setTitle(discardTitle, for: .normal)
            secondButton.setupBorder(UIColor.cancelColor(), radius: secondButton.frame.size.height / 2.0)
        } else {
            underlineWidth.constant = 0
            firstButton.setTitle(actions[0], for: .normal)
            firstButton.setupBorder(UIColor.okColor(), radius: 20)
            secondButton.setTitle(actions[1], for: .normal)
            secondButton.setupBorder(UIColor.okColor(), radius: 20)
            if thirdHandler != nil {
                thirdButton.setTitle(actions[2], for: .normal)
                thirdButton.setupBorder(UIColor.okColor(), radius: 20)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        if alertType == .threeChoices {
            containerHeight.constant = thirdHandler != nil ? 270 : 210
            thitdButtonHeight.constant = thirdButton != nil ? 40 : 0
        } else {
            let textSize = message.height(withConstrainedWidth: messageView.frame.width, font: messageView.font!)
            messageHeight.constant = textSize + 30
            let buttonsHeight:CGFloat = alertType == .oneChoice ? 60 : 120
            containerHeight.constant = 30 + messageHeight.constant + buttonsHeight
        }
    }
    
    @IBAction func pressOk(_ sender: Any) {
        dismiss(animated: true, completion: {
            self.presenter?.view.removeFromSuperview()
            if self.firstHandler != nil {
                self.firstHandler!()
            }
        })
    }
    
    @IBAction func pressOther(_ sender: Any) {
        dismiss(animated: true, completion: {
            self.presenter?.view.removeFromSuperview()
            if self.secondHandler != nil {
                self.secondHandler!()
            }
        })
    }
    
    @IBAction func pressThird(_ sender: Any) {
        dismiss(animated: true, completion: {
            self.presenter?.view.removeFromSuperview()
            if self.thirdHandler != nil {
                self.thirdHandler!()
            }
        })
    }
    
    @IBAction func pressCancel(_ sender: Any) {
        dismiss(animated: true, completion: {
            self.presenter?.view.removeFromSuperview()
            if self.cancelHandler != nil {
                self.cancelHandler!()
            }
        })
    }
    
#if LGAS_APP_EXTENSIONS
    
    class func message(_ host:UIViewController, title:String, message:String, okHandler:CompletionBlock? = nil) {
        let board = UIStoryboard(name: "Alerts", bundle: nil)
        if let controller = board.instantiateViewController(withIdentifier: "Alert") as? Alert {
            controller.alertType = .oneChoice
            controller.title = title.uppercased()
            controller.message = message
            controller.firstHandler = okHandler
            controller.modalTransitionStyle = .crossDissolve
            controller.modalPresentationStyle = .overCurrentContext
            TextFieldContainer.deactivateAll()
            host.present(controller, animated: true, completion: nil)
        }
    }
    
    class func question(_ host:UIViewController, title:String, message:String, okHandler:CompletionBlock? = nil, cancelHandler:CompletionBlock? = nil, okTitle:String = "Confirm", cancelTitle:String = "Discard") {
        let board = UIStoryboard(name: "Alerts", bundle: nil)
        if let controller = board.instantiateViewController(withIdentifier: "Alert") as? Alert {
            controller.alertType = .twoChoices
            controller.title = title.uppercased()
            controller.message = message
            controller.firstHandler = okHandler
            controller.secondHandler = cancelHandler
            controller.confirmTitle = okTitle
            controller.discardTitle = cancelTitle
            controller.modalTransitionStyle = .crossDissolve
            controller.modalPresentationStyle = .overCurrentContext
            TextFieldContainer.deactivateAll()
            host.present(controller, animated: true, completion: nil)
        }
    }

#else
 
    private func show() {
        TextFieldContainer.deactivateAll()
        
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overCurrentContext
        
        let mainWindow = UIApplication.shared.keyWindow
        self.presenter = UIViewController()
        self.presenter!.view.backgroundColor = UIColor.clear
        self.presenter!.view.isOpaque = false
        mainWindow?.addSubview(self.presenter!.view)
        self.presenter?.present(self, animated: true, completion: nil)
    }

    class func message(title:String, message:String, okHandler:CompletionBlock? = nil) {
        let board = UIStoryboard(name: "Alerts", bundle: nil)
        if let controller = board.instantiateViewController(withIdentifier: "Alert") as? Alert {
            controller.alertType = .oneChoice
            controller.title = title.uppercased()
            controller.message = message
            controller.firstHandler = okHandler
            controller.show()
        }
    }
    
    class func question(title:String, message:String, okHandler:CompletionBlock? = nil, cancelHandler:CompletionBlock? = nil, okTitle:String = "Confirm", cancelTitle:String = "Discard") {
        let board = UIStoryboard(name: "Alerts", bundle: nil)
        if let controller = board.instantiateViewController(withIdentifier: "Alert") as? Alert {
            controller.alertType = .twoChoices
            controller.title = title.uppercased()
            controller.message = message
            controller.firstHandler = okHandler
            controller.secondHandler = cancelHandler
            controller.confirmTitle = okTitle
            controller.discardTitle = cancelTitle
            controller.show()
        }
    }
    
    class func select(title:String, handlers:[AlertSelection], cancelHandler:CompletionBlock? = nil) {
        let board = UIStoryboard(name: "Alerts", bundle: nil)
        if let controller = board.instantiateViewController(withIdentifier: "Selection") as? Alert {
            controller.alertType = .threeChoices
            controller.title = title.uppercased()
            controller.firstHandler = handlers[0].handler
            controller.actions.append(handlers[0].name)
            controller.secondHandler = handlers[1].handler
            controller.actions.append(handlers[1].name)
            if handlers.count > 2 {
                controller.thirdHandler = handlers[2].handler
                controller.actions.append(handlers[2].name)
            }
            controller.cancelHandler = cancelHandler
            controller.show()
        }
    }
    
#endif
}

#if LGAS_APP_EXTENSIONS
#else

class TextAlert: UIViewController, TextFieldContainerDelegate {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var inputField: DefaultTextInputContainer!
    @IBOutlet weak var confirmField: DefaultTextInputContainer!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var discardButton: UIButton!
    
    enum AlertType {
        case text
        case email
    }

    private var alertType:AlertType = .text
    private var handler:CompletionTextBlock?
    private var presenter:UIViewController?
    private var defaultText:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        titleView.text = self.title

        confirmButton.setupBorder(UIColor.okColor(), radius: confirmButton.frame.size.height / 2.0)
        confirmButton.setTitle("confirm".localized, for: .normal)
        discardButton.setupBorder(UIColor.cancelColor(), radius: discardButton.frame.size.height / 2.0)
        discardButton.setTitle("discard".localized, for: .normal)

        if alertType == .email {
            inputField.textField.keyboardType = .emailAddress
            inputField.placeholder = "E-mail"
            inputField.textField.returnKeyType = .done
            inputField.delegate = self
        } else {
            inputField.textField.keyboardType = .default
            inputField.textField.autocorrectionType = .default
            inputField.textField.text = defaultText
            inputField.textField.returnKeyType = .done
            inputField.textField.autocapitalizationType = .words
            inputField.delegate = self
        }
        
        containerView.setupBorder(UIColor.clear, radius: 5)
        messageView.setupBorder(UIColor.errorColor(), radius: 20)
        messageView.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        inputField.activate(true)
    }
    
    @IBAction func pressOk(_ sender: Any) {
        if alertType == .email {
            acceptEmail()
        } else {
            acceptText()
        }
    }
    
    @IBAction func pressCancel(_ sender: Any) {
        TextFieldContainer.deactivateAll()
        dismiss(animated: true, completion: {
            self.presenter?.view.removeFromSuperview()
            if self.handler != nil {
                self.handler!(nil)
            }
        })
    }

    private func show() {
        TextFieldContainer.deactivateAll()
        
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overCurrentContext
        
        let mainWindow = UIApplication.shared.keyWindow
        self.presenter = UIViewController()
        self.presenter!.view.backgroundColor = UIColor.clear
        self.presenter!.view.isOpaque = false
        mainWindow?.addSubview(self.presenter!.view)
        self.presenter?.present(self, animated: true, completion: nil)
    }

    private func showErrorMessage(_ msg:String) {
        messageText.text = msg
        UIView.animate(withDuration: 0.5, animations: {
            self.messageView.alpha = 1
        }, completion: { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(2000), execute: {
                UIView.animate(withDuration: 0.5, animations: {
                    self.messageView.alpha = 0
                })
            })
        })
    }
    
    private func acceptPassword() {
        if inputField.text().length() < 8 {
            showErrorMessage("Password must be not less than 8 characters.")
        } else if inputField.text() != confirmField.text() {
            showErrorMessage("Password confirmation error.")
        } else {
            TextFieldContainer.deactivateAll()
            dismiss(animated: true, completion: {
                self.presenter?.view.removeFromSuperview()
                if self.handler != nil {
                    self.handler!(self.confirmField.text())
                }
            })
        }
    }
    
    private func acceptEmail() {
        if !inputField.text().isEmail() {
            showErrorMessage("Email should have xxxx@domain.prefix format.")
        } else {
            acceptText()
        }
    }
    
    private func acceptText() {
        TextFieldContainer.deactivateAll()
        dismiss(animated: true, completion: {
            self.presenter?.view.removeFromSuperview()
            if self.handler != nil {
                self.handler!(self.inputField.text())
            }
        })
    }
    
    func textDone(_ sender:TextFieldContainer, text:String?) {
        if alertType == .email {
            acceptEmail()
        } else {
            acceptText()
        }
    }
    
    func textChange(_ sender:TextFieldContainer, text:String?) -> Bool {
        return true
    }
    
    class func getEmail(_ handler:CompletionTextBlock?) {
        let board = UIStoryboard(name: "Alerts", bundle: nil)
        if let controller = board.instantiateViewController(withIdentifier: "TextAlert") as? TextAlert {
            controller.title = "ENTER NEW EMAIL"
            controller.alertType = .email
            controller.handler = handler
            controller.show()
        }
    }
    
    class func getText(title:String, defaultText:String? = nil, handler:CompletionTextBlock?) {
        let board = UIStoryboard(name: "Alerts", bundle: nil)
        if let controller = board.instantiateViewController(withIdentifier: "TextAlert") as? TextAlert {
            controller.title = title
            controller.alertType = .text
            controller.handler = handler
            if defaultText != nil {
                controller.defaultText = defaultText!
            }
            controller.show()
        }
    }

}

#endif
