//
//  AppDelegate.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 04.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import IQKeyboardManager
import SVProgressHUD

let UpdateWeatherNotification = Notification.Name("UpdateWeatherNotification")
let SyncDataNotification = Notification.Name("SyncDataNotification")

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        application.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)
        
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setBackgroundColor(UIColor.mainColor())
        SVProgressHUD.setForegroundColor(UIColor.white)
        SVProgressHUD.setFont(UIFont.condensedFont(15))
        
        UIApplication.shared.statusBarStyle = .lightContent
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font : UIFont.condensedFont(15)], for: .normal)
        IQKeyboardManager.shared().isEnableAutoToolbar = false

        return true
    }

    func mainController() -> MainController? {
        return window?.rootViewController as? MainController
    }

    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        if myPlaces().count > 0 {
            let next = NSCondition()
            DispatchQueue.global().async {
                for i in 0..<myPlaces().count {
                    if let place = myPlaceAtIndex(i) {
                        WeatherAPI.shared.currentWeather(place.id, value: { _, _ in
                            next.lock()
                            next.signal()
                            next.unlock()
                        })
                        next.lock()
                        next.wait()
                        next.unlock()
                    }
                }
                completionHandler(.newData)
            }
        } else {
            completionHandler(.noData)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: UpdateWeatherNotification, object: nil)
        NotificationCenter.default.post(name: SyncDataNotification, object: nil)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }


}

