//
//  DateRange.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 12.06.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

struct DateRange {
    let month:Int
    let year:Int
    let daysCount:Int
    
    init(month:Int, year:Int) {
        self.month = month
        self.year = year
        let dc = DateComponents(year:year, month: month)
        if let date = Calendar.current.date(from: dc) {
            let cal = Calendar(identifier: .gregorian)
            let monthRange = cal.range(of: .day, in: .month, for: date)!
            self.daysCount = monthRange.count
        } else {
            self.daysCount = 0
        }
    }
    
    init(year:Int) {
        self.month = 0
        self.year = year
        let dc = DateComponents(year:year)
        if let date = Calendar.current.date(from: dc) {
            let cal = Calendar(identifier: .gregorian)
            let yearRange = cal.range(of: .day, in: .year, for: date)!
            self.daysCount = yearRange.count
        } else {
            self.daysCount = 0
        }
    }
}
