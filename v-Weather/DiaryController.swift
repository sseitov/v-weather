//
//  DiaryController.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 08.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SVProgressHUD
import Charts

class DiaryController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, ChartViewDelegate {

    var place:SelectedPlace?
    
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var graph: LineChartView!
    
    private var currentRange:DateRange!
    private var dateFormatter:DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM"
        return formatter
    }
    
    private var backButton:UIBarButtonItem!
    private var statusButton:UIBarButtonItem!
    private var statusLabel:UILabel!
    private var forwardButton:UIBarButtonItem!
    private var currentIndex = 0
    private var activeDaysCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        place = myPlaceAtIndex(myPlaceIndex())
        setupTitle(place!.name!.uppercased())
        setupBackButton()
        
        let image = UIImage(named: "diary.jpg")
        self.view.layer.contents = image!.cgImage
        self.view.layer.contentsGravity = "resizeAspectFill"

        backButton = UIBarButtonItem(image: UIImage(named: "backButton"), style: .plain, target: self, action: #selector(self.moveBack))
        backButton.tintColor = .white
        backButton.isEnabled = false
        
        statusLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 100, height: 30))
        statusLabel.textColor = .white
        statusLabel.font = UIFont.thinFont(15)
        statusLabel.textAlignment = .center
        statusButton = UIBarButtonItem(customView: statusLabel)
        
        forwardButton = UIBarButtonItem(image: UIImage(named: "forwardButton"), style: .plain, target: self, action: #selector(self.moveForward))
        forwardButton.tintColor = .white
        forwardButton.isEnabled = false
        
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbarItems = [backButton, space, statusButton, space, forwardButton]

        graph.chartDescription?.enabled = false
        graph.delegate = self
        
        let xAxis = graph.xAxis
        xAxis.labelFont = .systemFont(ofSize: 11)
        xAxis.labelTextColor = .white
        xAxis.drawGridLinesEnabled = true
        xAxis.granularityEnabled = true
        
        let leftAxis = graph.leftAxis
        leftAxis.labelTextColor = .white
        leftAxis.drawGridLinesEnabled = true
        leftAxis.granularityEnabled = true

        graph.xAxis.labelPosition = XAxis.LabelPosition.bottom
        graph.legend.enabled = false

        let month = Calendar.current.component(.month, from: Date())
        picker.selectRow(month-1, inComponent: 0, animated: false)
        let year = Calendar.current.component(.year, from: Date())
        picker.selectRow(year - 2017, inComponent: 1, animated: false)

        currentRange = DateRange(month: month, year: year)
        refreshData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func goBack() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deletePlace(_ sender: Any) {
        let text = String(format: "Do you want to delete %@ from list?".localized, place!.name!.uppercased())
        Alert.question(title: "Delete".localized, message: text, okHandler: {
            deleteCurrentPlace()
            self.goBack()
        }, cancelHandler: {
            
        }, okTitle: "Delete".localized, cancelTitle: "Cancel".localized)
    }

    // MARK: - ChartView data
    
    private func refreshData() {
        
        func dayInFuture(_ day:Int = 1) -> Bool {
            let today = Date()
            let components = DateComponents(calendar: Calendar.current, timeZone: TimeZone.current, year: currentRange.year, month: currentRange.month, day: day)
            let date = Calendar.current.date(from: components)!
            return date > today
        }
        
        var hiEntries:[ChartDataEntry] = []
        var lowEntries:[ChartDataEntry] = []
        var hiPreviouseValue:Double = 0
        var lowPreviouseValue:Double = 0
        
        activeDaysCount = 0
        for day in 1...currentRange.daysCount {
            if dayInFuture(day) {
                break
            } else {
                activeDaysCount += 1
                let interval = Model.shared.placeWeatherForDay(place!.id, day: day, mongth: currentRange.month, year: currentRange.year)
                if interval.0 != nil {
                    let dataEntry = ChartDataEntry(x: Double(day), y: interval.0!)
                    hiEntries.append(dataEntry)
                    hiPreviouseValue = interval.0!
                } else {
                    let dataEntry = ChartDataEntry(x: Double(day), y: hiPreviouseValue)
                    hiEntries.append(dataEntry)
                }
                if interval.1 != nil {
                    let dataEntry = ChartDataEntry(x: Double(day), y: interval.1!)
                    lowEntries.append(dataEntry)
                    lowPreviouseValue = interval.1!
                } else {
                    let dataEntry = ChartDataEntry(x: Double(day), y: lowPreviouseValue)
                    lowEntries.append(dataEntry)
                }
            }
        }
        
        let hiChartDataSet = LineChartDataSet(values: hiEntries, label: "Hi")
        hiChartDataSet.setColor(UIColor.red)
        hiChartDataSet.axisDependency = .left
        hiChartDataSet.mode = .cubicBezier
        hiChartDataSet.drawCirclesEnabled = false
        hiChartDataSet.lineWidth = 3.0
        hiChartDataSet.highlightColor = .white
        hiChartDataSet.highlightEnabled = true
        hiChartDataSet.highlightLineWidth = 1
        hiChartDataSet.drawValuesEnabled = false
        hiChartDataSet.drawHorizontalHighlightIndicatorEnabled = false

        let lowChartDataSet = LineChartDataSet(values: lowEntries, label: "Low")
        lowChartDataSet.setColor(UIColor.blue)
        lowChartDataSet.axisDependency = .left
        lowChartDataSet.mode = .cubicBezier
        lowChartDataSet.drawCirclesEnabled = false
        lowChartDataSet.lineWidth = 3.0
        lowChartDataSet.highlightColor = .white
        lowChartDataSet.highlightEnabled = false
        lowChartDataSet.drawValuesEnabled = false
        lowChartDataSet.drawHorizontalHighlightIndicatorEnabled = false

        var dataSets:[IChartDataSet] = []
        dataSets.append(hiChartDataSet)
        dataSets.append(lowChartDataSet)
        
        let data = LineChartData(dataSets: dataSets)
        data.setValueTextColor(.white)
        data.setValueFont(.systemFont(ofSize: 9))

        graph.data = data
        if dayInFuture() {
            refreshToolbar()
        } else {
            graph.highlightValue(Highlight(x: 1, y: 0, dataSetIndex: 0), callDelegate: true)
        }
    }
    
    // MARK: - ChartView delegate

    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        let day = Int(entry.x)
        let components = DateComponents(calendar: Calendar.current, timeZone: TimeZone.current, year: currentRange.year, month: currentRange.month, day: day)
        if let date = Calendar.current.date(from: components) {
            let interval = Model.shared.placeWeatherForDay(place!.id, day: day, mongth: currentRange.month, year: currentRange.year)
            currentIndex = Int(entry.x)
            refreshToolbar(date: date, maxValue: interval.0, minValue: interval.1)
        } else {
            refreshToolbar()
        }
    }
    
    private func refreshToolbar(date:Date? = nil, maxValue:Double? = nil, minValue:Double? = nil) {
        if date == nil {
            let text = "This day is in future".localized
            statusButton.width = text.width(withConstrainedHeight: 30, font: statusLabel.font)
            statusLabel.text = text
            backButton.isEnabled = false
            forwardButton.isEnabled = false
        } else {
            var text = "\(dateFormatter.string(from: date!)) ".uppercased()
            if maxValue != nil || minValue != nil {
                if maxValue != nil {
                    text += " \("Maximum".localized.lowercased()) \(Int(maxValue!))ºC"
                }
                if minValue != nil {
                    text += " \("Minimum".localized.lowercased()) \(Int(minValue!))ºC"
                }
            } else {
                text += "No Data".localized.uppercased()
            }
            statusButton.width = text.width(withConstrainedHeight: 30, font: statusLabel.font)
            statusLabel.text = text
            
            if currentRange.daysCount > 1 {
                backButton.isEnabled = currentIndex > 1
                forwardButton.isEnabled = currentIndex < activeDaysCount
            } else {
                backButton.isEnabled = false
                forwardButton.isEnabled = false
            }
        }
    }
    
    @objc
    func moveBack() {
        graph.highlightValue(Highlight(x: Double(currentIndex - 1), y: 0, dataSetIndex: 0), callDelegate: true)
    }
    
    @objc
    func moveForward() {
        graph.highlightValue(Highlight(x: Double(currentIndex + 1), y: 0, dataSetIndex: 0), callDelegate: true)
    }

    // MARK: - UIPickerView extensions
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return component == 0 ? 13 : 100
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return component == 0 ? 140 : 100
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attr:[NSAttributedStringKey:Any] = [.foregroundColor : UIColor.white]
        if component == 0 {
            if row < 12 {
                return NSAttributedString(string: DateFormatter().standaloneMonthSymbols[row], attributes: attr)
            } else {
                return NSAttributedString(string: "allYear".localized, attributes: attr)
            }
        } else {
            return NSAttributedString(string: "\(row + 2017)", attributes: attr)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let month = self.picker.selectedRow(inComponent: 0) + 1
        let year = self.picker.selectedRow(inComponent: 1) + 2017
        if row < 12 {
            currentRange = DateRange(month: month, year: year)
        } else {
            currentRange = DateRange(year: year)
        }
        refreshData()
    }

}
