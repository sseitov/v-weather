//
//  ForecastCell.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 11.06.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SDWebImage

class ForecastCell: UITableViewCell {

    @IBOutlet weak var dateView: UILabel!
    @IBOutlet weak var descriptionView: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var dayView: UILabel!
    @IBOutlet weak var nightView: UILabel!

    var forecast:[String:Any]? {
        didSet {
            if forecast != nil {
                if let dt = forecast!["dt"] as? Double {
                    let date = Date(timeIntervalSince1970: dt)
                    dateView.text = weekDateFormatter().string(from: date)
                }
                if let temp = forecast!["temp"] as? [String:Any], let day = temp["day"] as? Double, let night = temp["night"] as? Double {
                    dayView.text = "\(Int((day).rounded()))\u{2103}"
                    nightView.text = "\(Int(night.rounded()))\u{2103}"
                }
                if let weatherData = forecast!["weather"] as? [Any], let item = weatherData.first as? [String:Any], let icon = item["icon"] as? String, let url = URL(string: "http://openweathermap.org/img/w/\(icon).png"), let descr = item["description"] as? String
                {
                    descriptionView.text = descr
                    if let image = SDImageCache.shared().imageFromCache(forKey: url.absoluteString) {
                        iconView.image = image
                    } else {
                        SDWebImageDownloader.shared().downloadImage(
                            with: url, options: [], progress: nil,
                            completed: { newImage, _, _, _ in
                                if newImage != nil {
                                    SDImageCache.shared().store(newImage, forKey: url.absoluteString, completion: {
                                        self.iconView.image = newImage
                                    })
                                }
                        })
                    }
                }
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        dayView.backgroundColor = UIColor.red
        dayView.setupBorder(UIColor.red, radius: 5)
        nightView.backgroundColor = UIColor.black
        nightView.setupBorder(UIColor.black, radius: 5)
    }
}
