//
//  ForecastController.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 11.06.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit

class ForecastController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var forecastTable: UITableView!
    @IBOutlet weak var progress: UIActivityIndicatorView!
    
    var placeID:Int32 = 0
    private var forecast:[Any] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let image = UIImage(named: "logo.jpg")
        self.view.layer.contents = image!.cgImage
        self.view.layer.contentsGravity = "resizeAspectFill"

        titleView.text = "forecastLong".localized
        progress.startAnimating()
        WeatherAPI.shared.longForecast(placeID, values: { result, error in
            self.progress.stopAnimating()
            if error != nil {
                Alert.message(title: "error".localized, message: error!.localizedDescription)
            } else {
                self.forecast = result
                self.forecastTable.reloadData()
            }
        })
    }

    @IBAction func done(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecast.count > 0 ? forecast.count - 1 : 0
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "forecast", for: indexPath) as! ForecastCell
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        cell.forecast = forecast[indexPath.row+1] as? [String:Any]
        return cell
    }
}
