//
//  MainController.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 07.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SVProgressHUD

class MainController: UIViewController {

    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var controlIndicator: UIPageControl!

    var placeControl:UIPageViewController?
    var currentPlace:WeatherController?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let image = UIImage(named: "logo.jpg")
        self.view.layer.contents = image!.cgImage
        self.view.layer.contentsGravity = "resizeAspectFill"
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.iCloudSync),
                                               name: SyncDataNotification,
                                               object: nil)

    }
    
    @objc
    func iCloudSync() {
        func doSync() -> Bool {
            if let lastDate = UserDefaults.standard.object(forKey: "lastSyncDate") as? Date {
                return Date().timeIntervalSince(lastDate) > 60*60*24    // more then one day
            } else {
                return true
            }
        }
        
        if myPlaces().count > 0, doSync() {
            SVProgressHUD.show(withStatus: "iCloud Sync...")
            SyncManager.shared.refreshCloud({ error in
                SVProgressHUD.dismiss()
                if error != nil {
                    Alert.message(title: "error".localized, message: error!)
                } else {
                    UserDefaults.standard.set(Date(), forKey: "lastSyncDate")
                }
            })
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if myPlaces().count == 0 {
            performSegue(withIdentifier: "newPlace", sender: nil)
        } else {
            currentPlace = getPlaceAtIndex(myPlaceIndex())
            placeControl?.setViewControllers([currentPlace!], direction: .forward, animated: false, completion: { _ in
                self.placeName.text = self.currentPlace!.place!.name?.uppercased()
                self.controlIndicator.numberOfPages = myPlaces().count
                self.controlIndicator.currentPage = myPlaceIndex()
            })
        }
    }
    
    func getPlaceAtIndex(_ index: Int) -> WeatherController {
        let placeContent = self.storyboard?.instantiateViewController(withIdentifier: "WeatherController") as! WeatherController
        if let selected = myPlaceAtIndex(index) {
            placeContent.place = Model.shared.getPlace(selected.id)
        }
        placeContent.index = index
        return placeContent
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pageControl" {
            placeControl = segue.destination as? UIPageViewController
            placeControl?.dataSource = self
            placeControl?.delegate = self
        }
    }

}

extension MainController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed else { return }
        self.currentPlace = pageViewController.viewControllers!.first as? WeatherController
        self.placeName.text = self.currentPlace!.place!.name?.uppercased()
        self.controlIndicator.currentPage = self.currentPlace!.index
        setMyPlaceIndex(self.currentPlace!.index)
    }
}

extension MainController : UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        let placeContent = viewController as! WeatherController
        var index = placeContent.index
        if (index == NSNotFound) {
            return nil;
        }
        index += 1;
        if (index == myPlaces().count) {
            return nil;
        }
        return getPlaceAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let placeContent = viewController as! WeatherController
        var index = placeContent.index
        if ((index == 0) || (index == NSNotFound)) {
            return nil
        }
        index -= 1;
        return getPlaceAtIndex(index)
    }
}
