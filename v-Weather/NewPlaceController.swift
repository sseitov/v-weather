//
//  NewPlaceController.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 05.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SVProgressHUD
import CoreLocation
import MapKit

class NewPlaceController: UIViewController, UISearchBarDelegate, MKMapViewDelegate {

    @IBOutlet weak var map: MKMapView!
    
    private var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 300, height: 30))
    private var selectedPlace:Place?
    private var annotation:MKPointAnnotation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBackButton()
        
        searchBar.delegate = self
        searchBar.placeholder = "your place name"
        navigationItem.titleView = searchBar
    }
    
    @IBAction func getLocation() {
        searchBar.resignFirstResponder()
        SVProgressHUD.show(withStatus: "Find...")
        LocationManager.shared.updateLocation({ location in
            if location != nil {
                WeatherAPI.shared.findCity(location!.coordinate, value: { place, error in
                    SVProgressHUD.dismiss()
                    self.selectPlace(place)
                })
            } else {
                SVProgressHUD.dismiss()
                self.selectedPlace = nil
            }
        })
    }
    
    override func goBack() {
        SVProgressHUD.dismiss()
        if selectedPlace != nil {
            TextAlert.getText(title: "Do you want to add this place?".localized, defaultText: selectedPlace?.name, handler: { name in
                if name != nil {
                    self.selectedPlace?.name = name
                    addMyPlace(self.selectedPlace!)
                    
                    SVProgressHUD.show(withStatus: "iCloud Sync...")
                    SyncManager.shared.refreshPlaceWeather({ success in
                        SVProgressHUD.dismiss()
                        if !success {
                            Alert.message(title: "error".localized, message: "networkError".localized, okHandler: {
                                self.dismiss(animated: true, completion: nil)
                            })
                        } else {
                            self.dismiss(animated: true, completion: nil)
                        }
                    })
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            })
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func selectPlace(_ place:Place?) {
        self.selectedPlace = place
        if place != nil {
            if annotation != nil {
                map.removeAnnotation(annotation!)
                annotation = nil
                let worldRegion = MKCoordinateRegionForMapRect(MKMapRectWorld)
                self.map.setRegion(worldRegion, animated: true)
            } else {
                annotation = MKPointAnnotation()
                annotation!.coordinate = place!.coordinate
                let region = MKCoordinateRegionMakeWithDistance( place!.coordinate, 1000000, 1000000)
                self.map.addAnnotation(annotation!)
                self.map.setRegion(region, animated: true)
            }
        }
    }
    
    // MARK: - UISearchBar Delegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        SVProgressHUD.show(withStatus: "Find...")
        CLGeocoder().geocodeAddressString(searchBar.text!, completionHandler: { places, error in
            if let place = places?.first {
                WeatherAPI.shared.findCity(place.location!.coordinate, value: { place, error in
                    SVProgressHUD.dismiss()
                    self.selectPlace(place)
                })
            } else {
                SVProgressHUD.dismiss()
                self.selectedPlace = nil
            }
        })
    }

    // MARK: - MKMapView Delegate

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if annotation == nil && selectedPlace != nil {
            annotation = MKPointAnnotation()
            annotation!.coordinate = selectedPlace!.coordinate
            let region = MKCoordinateRegionMakeWithDistance( selectedPlace!.coordinate, 1000000, 1000000)
            self.map.addAnnotation(annotation!)
            self.map.setRegion(region, animated: true)
        }
    }
}
