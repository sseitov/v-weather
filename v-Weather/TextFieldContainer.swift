//
//  TextFieldContainer.swift
//  cryptoBox (MilCryptor Secure Platform)
//
//  Created by Denys Borysiuk on 19.07.16.
//  Copyright © 2016 ArchiSec Solutions, Ltd. All rights reserved.//
//

import UIKit

protocol TextFieldContainerDelegate:class {
    func textDone(_ sender:TextFieldContainer, text:String?)
    func textChange(_ sender:TextFieldContainer, text:String?) -> Bool
}

class TextFieldContainer: UIView, UITextFieldDelegate {

    weak var delegate:TextFieldContainerDelegate?
    
    var nonActiveColor:UIColor = UIColor.mainColor()
    var activeColor:UIColor = UIColor.mainDarkColor()
    var placeholderColor = UIColor.mainBorderColor()
    
    var placeholder: String = "" {
        didSet {
            textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedStringKey.foregroundColor : placeholderColor])
        }
    }
    var textField:UITextField!

    var keyboardHeight: CGFloat!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField = UITextField(frame: self.bounds.insetBy(dx: 10, dy: 3))
        textField.textAlignment = .center
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.tintColor = UIColor.white
        textField.delegate = self

        if #available(iOS 11, *) {
            // Disables the password autoFill accessory view.
            textField.textContentType = UITextContentType("")
        }
 
        configure()

        self.addSubview(textField)
    }
    
    class func deactivateAll() {
        #if STANDALONE
        UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
        #endif
    }

#if LGAS_APP_EXTENSIONS
    class func inputError(_ host:UIViewController, error:String, responder:TextFieldContainer) {
        Alert.message(host, title: "Input Error".uppercased(), message: error, okHandler: {
            responder.activate(true)
        })
    }
#else
    class func inputError(error:String, responder:TextFieldContainer) {
        Alert.message(title: "Input Error".uppercased(), message: error, okHandler: {
            responder.activate(true)
        })
    }
#endif
    
    func configure() {
        backgroundColor = nonActiveColor
        setupBorder(UIColor.white, radius: 5)
        textField.textColor = UIColor.white
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        textField.frame = self.bounds.insetBy(dx: 10, dy: 3)
    }
    
    func activate(_ active:Bool) {
        if active {
            textField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }
    
    func text() -> String {
        return textField.text == nil ? "" : textField.text!
    }
    
    func setText(_ text:String) {
        textField.text = text
    }
    
    func clear() {
        textField.text = ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText:String? = textField.text != nil ? (textField.text! as NSString).replacingCharacters(in: range, with: string) : nil
        if newText == nil || newText!.isEmpty {
            backgroundColor = nonActiveColor
        } else {
            backgroundColor = activeColor
        }
        if string == "\n" {
            textField.resignFirstResponder()
            delegate?.textDone(self, text: textField.text)
            return false
        } else {
            if delegate != nil {
                return delegate!.textChange(self, text: newText)
            } else {
                return true
            }
        }
    }
    
}

class DefaultTextInputContainer : TextFieldContainer {
    
    override func configure() {
        backgroundColor = UIColor.clear
        textField.font = UIFont(name: "SFUIDisplay-Regular", size: 15)
        textField.textColor = UIColor.white
        textField.textAlignment = .left
        placeholderColor = UIColor.lightGray
        textField.borderStyle = .none
        nonActiveColor = UIColor.clear
        activeColor = UIColor.clear
    }
}
