//
//  WeatherAPI.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 07.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import Foundation
import AFNetworking
import CoreLocation

func weatherError(_ text:String) -> NSError {
    return NSError(domain: "com.vchannel.v-weather", code: -1, userInfo: [NSLocalizedDescriptionKey:text])
}

func windDirection(_ degree:Double) -> String {    
    if degree >  348.75 && degree <= 11.25 {
        return "N".localized
    } else if degree >  11.25 && degree <= 33.75 {
        return "NNE".localized
    } else if degree >  33.75 && degree <= 56.25 {
        return "NE".localized
    } else if degree >  56.25 && degree <= 78.75 {
        return "ENE".localized
    } else if degree >  78.75 && degree <= 101.25 {
        return "E".localized
    } else if degree >  101.25 && degree <= 123.75 {
        return "ESE".localized
    } else if degree >  123.75 && degree <= 146.25 {
        return "SE".localized
    } else if degree >  146.25 && degree <= 168.75 {
        return "SSE".localized
    } else if degree >  168.75 && degree <= 191.25 {
        return "S".localized
    } else if degree >  191.25 && degree <= 213.75 {
        return "SSW".localized
    } else if degree >  213.75 && degree <= 236.25 {
        return "SW".localized
    } else if degree >  236.25 && degree <= 258.75 {
        return "WSW".localized
    } else if degree >  258.75 && degree <= 281.25 {
        return "W".localized
    } else if degree >  281.25 && degree <= 303.75 {
        return "WNW".localized
    } else if degree >  303.75 && degree <= 326.25 {
        return "NW".localized
    } else { //if degree >  326.25 && degree <= 348.75 {
        return "NNW".localized
    }
}

class WeatherAPI: NSObject {
    
    fileprivate lazy var httpManager:AFHTTPSessionManager = {
        let manager = AFHTTPSessionManager(baseURL: URL(string: "http://api.openweathermap.org/data/2.5/"))
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        manager.responseSerializer = AFJSONResponseSerializer()
        return manager
    }()

    static let shared = WeatherAPI()
    
    private override init() {
        super.init()
    }
    
    func findCity(_ coord:CLLocationCoordinate2D, value:@escaping (Place?, Error?) -> ()) {
        let params:[String:Any] = ["lat" : coord.latitude,
                                   "lon" : coord.longitude,
                                   "sort" : "population",
                                   "cnt" : 50,
                                   "APPID" : OpenWeatherMapKey]
        
        httpManager.get("find", parameters: params, progress: nil, success: { _, result in
            if let data = result as? [String:Any],
                let list = data["list"] as? [Any],
                let city = list.first as? [String:Any],
                let id = city["id"] as? Int32,
                let place = Model.shared.getPlace(id)
            {
                value(place, nil)
            }
        }, failure: { _, error in
            value(nil, error)
        })
    }
    
    func currentWeather(_ placeID:Int32, value:@escaping (Weather?, Error?) -> ()) {
        var params:[String:Any] = ["id" : placeID, "units" : "metric", "APPID" : OpenWeatherMapKey]
        if Locale.current.identifier == "ru_RU" {
            params["lang"] = "ru"
        }
        httpManager.get("weather", parameters: params, progress: nil, success: { _, result in
            if let data = result as? [String:Any] {
                if let main = data["main"] as? [String:Any], let temperature = main["temp"] as? Double, let pressure = main["pressure"] as? Double, let humidity = main["humidity"] as? Double
                {
                    let weather = Model.shared.createWeather(date: Date().timeIntervalSince1970, place: placeID, isForecast: false)
                    weather.placeID = placeID
                    weather.temperature = temperature
                    weather.pressure = pressure
                    weather.humidity = humidity
                    if let weatherData = data["weather"] as? [Any], let item = weatherData.first as? [String:Any], let icon = item["icon"] as? String, let descr = item["description"] as? String
                    {
                        weather.stateDescription = descr
                        weather.stateIcon = icon
                    }
                    if let wind = data["wind"] as? [String:Any], let speed = wind["speed"] as? Double, let deg = wind["deg"] as? Double
                    {
                        weather.windSpeed = speed
                        weather.windDirection = deg
                    }
                    Model.shared.saveContext()
                    value(weather, nil)
                } else {
                    value(nil, nil)
                }
            } else {
                value(nil, nil)
            }
        }, failure: { _, error in
            value(nil, error)
        })
    }

    func todayForecast(_ placeID:Int32, value:@escaping ([Weather], Error?) -> ()) {
        var params:[String:Any] = ["id" : placeID, "units" : "metric", "APPID" : OpenWeatherMapKey]
        if Locale.current.identifier == "ru_RU" {
            params["lang"] = "ru"
        }
        httpManager.get("forecast", parameters: params, progress: nil, success: { _, result in
            if let data = result as? [String:Any], let list = data["list"] as? [Any] {
                var values:[Weather] = []
                for item in list {
                    if let value = item as? [String:Any],
                        let dt = value["dt"] as? Double,
                        (Date(timeIntervalSince1970: dt).isToday() || Date(timeIntervalSince1970: dt).isTomorrow()),
                        let main = value["main"] as? [String:Any],
                        let temperature = main["temp"] as? Double,
                        let pressure = main["pressure"] as? Double,
                        let humidity = main["humidity"] as? Double
                    {
                        let weather = Model.shared.createWeather(date: dt, place: placeID, isForecast: true)
                        weather.temperature = temperature
                        weather.pressure = pressure
                        weather.humidity = humidity
                        if let weatherData = value["weather"] as? [Any], let item = weatherData.first as? [String:Any], let icon = item["icon"] as? String, let descr = item["description"] as? String
                        {
                            weather.stateDescription = descr
                            weather.stateIcon = icon
                        }
                        if let wind = value["wind"] as? [String:Any], let speed = wind["speed"] as? Double, let deg = wind["deg"] as? Double
                        {
                            weather.windSpeed = speed
                            weather.windDirection = deg
                        }

                        Model.shared.saveContext()
                        values.append(weather)
                    }
                }
                Model.shared.saveContext()
                value(values, nil)
            } else {
                value([], weatherError("Invalid data format"))
            }
        }, failure: { _, error in
            value([], error)
        })
    }

    func longForecast(_ placeID:Int32, values:@escaping ([Any], Error?) -> ()) {
        var params:[String:Any] = ["id" : placeID, "units" : "metric", "APPID" : OpenWeatherMapKey, "cnt" : 11]
        if Locale.current.identifier == "ru_RU" {
            params["lang"] = "ru"
        }
        httpManager.get("forecast/daily", parameters: params, progress: nil, success: { _, result in
            if let data = result as? [String:Any], let list = data["list"] as? [Any] {
                values(list, nil)
            } else {
                values([], weatherError("Invalid data format"))
            }
        }, failure: { _, error in
            values([], error)
        })
    }
}
