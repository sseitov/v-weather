//
//  WeatherCell.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 05.06.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SDWebImage

class WeatherCell: UITableViewCell {

    @IBOutlet weak var dateView: UILabel!
    @IBOutlet weak var descriptionView: UILabel!
    @IBOutlet weak var tempView: UILabel!
    @IBOutlet weak var pressureView: UILabel!
    @IBOutlet weak var humidityView: UILabel!
    @IBOutlet weak var windView: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    
    var weather:Weather? {
        didSet {
            dateView.text = hourlyFormatter().string(from: Date(timeIntervalSince1970: weather!.date))
            descriptionView.text = weather!.stateDescription
            tempView.text = "\(Int(weather!.temperature.rounded()))\u{2103}"
            let pressure = weather!.pressure / 1.3332239
            pressureView.text = "\(Int(pressure.rounded())) mm"
            humidityView.text = "\(Int(weather!.humidity)) %"
            windView.text = "\(windDirection(weather!.windDirection)) \(String.init(format: "%.1f m/s", weather!.windSpeed))"

            if weather!.stateIcon != nil, let url = URL(string: "http://openweathermap.org/img/w/\(weather!.stateIcon!).png") {
                if let image = SDImageCache.shared().imageFromCache(forKey: url.absoluteString) {
                    iconView.image = image
                } else {
                    SDWebImageDownloader.shared().downloadImage(
                        with: url, options: [], progress: nil,
                        completed: { newImage, _, _, _ in
                            if newImage != nil {
                                SDImageCache.shared().store(newImage, forKey: url.absoluteString, completion: {
                                    self.iconView.image = newImage
                                })
                            }
                    })
                }
            }
        }
    }
    
}
