//
//  WeatherController.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 07.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class WeatherController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var index:Int = 0
    var place:Place?
    
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var weatherData: UITableView!
    @IBOutlet weak var forecastData: UITableView!
    @IBOutlet weak var forecastButton: UIButton!
    
    private var weather: Weather?
    private var forecast:[Weather] = []
    private var weatherIcon: UIImage?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        forecastButton.setTitle("forecastLong".localized, for: .normal)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.refresh),
                                               name: UpdateWeatherNotification,
                                               object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refresh()
    }

    @objc
    func refresh() {
        if place == nil {
            return
        }
        
        weather = Model.shared.lastPlaceWeather(place!.id)
        if weather == nil || !weather!.isActual() {
            if SyncManager.shared.networkAvailable() {
                let weatherProgress = UIActivityIndicatorView(activityIndicatorStyle: .white)
                weatherProgress.center = CGPoint(x: self.weatherData.bounds.width / 2.0, y: self.weatherData.bounds.height / 2.0)
                temperature.addSubview(weatherProgress)
                weatherProgress.startAnimating()
                WeatherAPI.shared.currentWeather(place!.id, value: { weather, error in
                    weatherProgress.removeFromSuperview()
                    self.weather = weather
                    self.updateData()
                })
            } else {
                Alert.message(title: "error".localized, message: "networkError".localized, okHandler: {
                    if self.weather != nil {
                        self.updateData()
                    }
                })
            }
        } else {
            updateData()
        }
        
        forecast = Model.shared.lastPlaceForecast(place!.id)
        if forecast.count < 10 {
            let forecastProgress = UIActivityIndicatorView(activityIndicatorStyle: .white)
            forecastProgress.center = self.forecastData.center
            self.view.addSubview(forecastProgress)
            forecastProgress.startAnimating()
            WeatherAPI.shared.todayForecast(place!.id, value: { days, error in
                forecastProgress.removeFromSuperview()
                if error != nil {
                    Alert.message(title: "error".localized, message: error!.localizedDescription)
                } else {
                    self.forecast = days
                    self.forecastData.reloadData()
                }
            })
        } else {
            forecastData.reloadData()
        }
 
    }
    
    func updateData() {
        temperature.text = "\(Int(weather!.temperature.rounded()))\u{2103}"
        weatherData.reloadData()
        
        if weather!.stateIcon != nil {
            if let url = URL(string: "http://openweathermap.org/img/w/\(weather!.stateIcon!).png") {
                if let image = SDImageCache.shared().imageFromCache(forKey: url.absoluteString) {
                    self.weatherIcon = image
                    self.weatherData.reloadData()
                } else {
                    SDWebImageDownloader.shared().downloadImage(
                        with: url, options: [], progress: nil,
                        completed: { newImage, _, _, _ in
                            if newImage != nil {
                                SDImageCache.shared().store(newImage, forKey: url.absoluteString, completion: {
                                    self.weatherIcon = newImage
                                    self.weatherData.reloadData()
                                })
                            }
                    })
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == weatherData {
            return weather == nil ? 0 : 4
        } else {
            return forecast.count
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == weatherData {
            if indexPath.row == 0 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
                cell.backgroundColor = UIColor.clear
                cell.contentView.backgroundColor = UIColor.clear
                cell.textLabel?.font = UIFont.mainFont(17)
                cell.textLabel?.textColor = UIColor.white
                cell.accessoryView = UIImageView(image: weatherIcon)
                cell.textLabel?.text = self.weather!.stateDescription
                return cell
            } else {
                let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)
                cell.backgroundColor = UIColor.clear
                cell.contentView.backgroundColor = UIColor.clear
                cell.textLabel?.font = UIFont.mainFont(17)
                cell.detailTextLabel?.font = UIFont.condensedFont(21)
                cell.textLabel?.textColor = UIColor.white
                cell.detailTextLabel?.textColor = UIColor.white
                if indexPath.row == 1 {
                    cell.textLabel?.text = "pressure".localized
                    if self.weather != nil {
                        let pressure = self.weather!.pressure / 1.3332239
                        cell.detailTextLabel?.text = "\(Int(pressure.rounded())) mm"
                    }
                } else if indexPath.row == 2 {
                    cell.textLabel?.text = "humidity".localized
                    if self.weather != nil {
                        cell.detailTextLabel?.text = "\(Int(self.weather!.humidity)) %"
                    }
                } else {
                    cell.textLabel?.text = "wind".localized
                    if self.weather != nil {
                        cell.detailTextLabel?.text = "\(windDirection(self.weather!.windDirection)) \(String.init(format: "%.1f m/s", self.weather!.windSpeed))"
                    }
                }
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "weather", for: indexPath) as! WeatherCell
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            cell.weather = forecast[indexPath.row]
            return cell
        }
    }
    
    // MARK: - Navigation

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "ForecastController" {
            return place != nil
        } else {
            return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showForecast" {
            let next = segue.destination as! ForecastController
            next.placeID = place!.id
        }
    }

}
