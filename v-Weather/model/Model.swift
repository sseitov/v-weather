//
//  Model.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 05.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//


import UIKit
import CoreData
import CoreLocation

class Model: NSObject {
    
    static let shared = Model()
    
    private override init() {
        super.init()
    }
    
    // MARK: - CoreData stack
    
    lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "Model", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("Model.sqlite")
        let placeUrl = self.applicationDocumentsDirectory.appendingPathComponent("Place.sqlite")
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true])
            if !FileManager.default.fileExists(atPath: placeUrl.path) {
                let bundlePlaceUrl = Bundle.main.url(forResource: "Place", withExtension: "sqlite")
                try FileManager.default.copyItem(at: bundlePlaceUrl!, to: placeUrl)
            }
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: placeUrl, options: [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true])
        } catch {
            print("CoreData data error: \(error)")
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                print("Saved data error: \(error)")
            }
        }
    }
    
    // MARK: - Place table
    
    func getPlace(_ id:Int32) -> Place? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Place")
        fetchRequest.predicate = NSPredicate(format: "id = %d", id)
        do {
            return try managedObjectContext.fetch(fetchRequest).first as? Place
        } catch {
            return nil
        }
    }
    
    func placesByName(_ name:String) -> [Place] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Place")
        fetchRequest.predicate = NSPredicate(format: "name BEGINSWITH[c] %@", name)
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.fetchLimit = 100
        if let all = try? managedObjectContext.fetch(fetchRequest) as! [Place] {
            return all
        } else {
            return []
        }
    }
    
    func placesByCoordinate(_ coord:CLLocationCoordinate2D) -> [Place] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Place")
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        if let all = try? managedObjectContext.fetch(fetchRequest) as! [Place] {
            var filtered = all.filter({ place in
                return (place.distance(from: coord) < 1000)
            })
            if filtered.count == 0 {
                let place = NSEntityDescription.insertNewObject(forEntityName: "Place", into: managedObjectContext) as! Place
                place.latitude = coord.latitude
                place.longitude = coord.longitude
                let randomNum:UInt32 = arc4random_uniform(UINT32_MAX)
                place.id = -Int32(randomNum)
                place.name = String(format: "Latitude %.2f, Longitude %.2f", coord.latitude, coord.longitude)
                place.country = ""
                saveContext()
                filtered.append(place)
            }
            return filtered
        } else {
            return []
        }
    }
    
    // MARK: - Weather table
    
    func allWeather() -> [Weather] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        if let value = try? managedObjectContext.fetch(fetchRequest) as? [Weather] {
            if value != nil {
                return value!
            } else {
                return []
            }
        } else {
            return []
        }
    }
    
    func allWeatherCount() -> Int {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        if let value = try? managedObjectContext.fetch(fetchRequest) {
            return value.count
        } else {
            return 0
        }
    }

    func allSyncedWeatherCount() -> Int {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        if let all = try? managedObjectContext.fetch(fetchRequest) as? [Weather] {
            if all != nil {
                let synced = all!.filter({ weather in return weather.synced})
                return synced.count
            } else {
                return 0
            }
        } else {
            return 0
        }
    }

    func deleteWeather(_ weather:Weather) {
        managedObjectContext.delete(weather)
        saveContext()
    }
    
    func getWeather(_ recordName:String) -> Weather? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        fetchRequest.predicate = NSPredicate(format: "recordName == %@", recordName)
        if let value = try? managedObjectContext.fetch(fetchRequest).first as? Weather {
            return value
        } else {
            return nil
        }
    }
    
    func unsyncedWeather() -> [Weather] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        if let all = try? managedObjectContext.fetch(fetchRequest) as? [Weather] {
            if all != nil {
                let synced = all!.filter({ weather in return !weather.synced})
                return synced
            } else {
                return []
            }
        } else {
            return []
        }
    }
    
    func createWeather(date:Double, place: Int32, isForecast:Bool) -> Weather {
        let weather = NSEntityDescription.insertNewObject(forEntityName: "Weather", into: managedObjectContext) as! Weather
        weather.date = date
        weather.placeID = place
        weather.forecast = isForecast
        return weather
    }

    func placeWeatherData(_ id:Int32) -> [Weather] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        fetchRequest.predicate = NSPredicate(format: "placeID = %d", id)
        if let all = try? managedObjectContext.fetch(fetchRequest) as? [Weather] {
            if all != nil {
                return all!
            } else {
                return []
            }
        } else {
            return []
        }
    }
    
    func allPlaceWeatherDataBeforeDate(_ date:Date, place:Int32) -> [Weather] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        fetchRequest.predicate = NSPredicate(format: "placeID = %d", place)
        if let all = try? managedObjectContext.fetch(fetchRequest) as? [Weather] {
            if all != nil {
                return all!
            } else {
                return []
            }
        } else {
            return []
        }
    }
    
    func firstDate() -> Date? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        let sort = NSSortDescriptor(key: "date", ascending: true)
        fetchRequest.sortDescriptors = [sort]
        fetchRequest.fetchLimit = 1
        if let weather = try? managedObjectContext.fetch(fetchRequest).first as? Weather {
            if weather != nil {
                return Date(timeIntervalSince1970: weather!.date)
            }
        }
        return nil
    }

    func lastPlaceWeather(_ id:Int32) -> Weather? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        let pred1 = NSPredicate(format: "placeID == %d", id)
        let pred2 = NSPredicate(format: "forecast == NO")
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [pred1, pred2])
        let sort = NSSortDescriptor(key: "date", ascending: false)
        fetchRequest.sortDescriptors = [sort]
        fetchRequest.fetchLimit = 1
        if let value = try? managedObjectContext.fetch(fetchRequest).first as? Weather {
            return value
        } else {
            return nil
        }
    }
    
    func lastPlaceForecast(_ id:Int32) -> [Weather] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        let pred1 = NSPredicate(format: "placeID == %d", id)
        let pred2 = NSPredicate(format: "forecast == YES")
        let pred3 = NSPredicate(format: "date > %lf", Date().timeIntervalSince1970)
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [pred1, pred2, pred3])
        let sort = NSSortDescriptor(key: "date", ascending: true)
        fetchRequest.sortDescriptors = [sort]
        if let all = try? managedObjectContext.fetch(fetchRequest) as! [Weather] {
            var values:[Weather] = []
            for w in all {
                let d = Date(timeIntervalSince1970: w.date)
                if d.isToday() || d.isTomorrow(){
                    values.append(w)
                }
            }
            return values
        } else {
            return []
        }
    }

    func maxMinWeatherForDay(_ id:Int32, date:Date) -> (Weather?, Weather?, [Weather]) {
        let end = date.endOfDay!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
        let pred1 = NSPredicate(format: "placeID == %d", id)
        let pred2 = NSPredicate(format: "date >= %lf", date.startOfDay.timeIntervalSince1970)
        let pred3 = NSPredicate(format: "date < %lf", end.timeIntervalSince1970)
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [pred1, pred2, pred3])
        let sort = NSSortDescriptor(key: "date", ascending: true)
        fetchRequest.sortDescriptors = [sort]
        
        if let all = try? managedObjectContext.fetch(fetchRequest) as? [Weather] {
            if all != nil {
                let synced = all!.filter({ w in return w.synced })
                if synced.count > 1 {
                    var temp:[Double:Weather] = [:]
                    for w in synced {
                        temp[w.temperature] = w
                    }
                    let sort = temp.keys.sorted()
                    if sort.last != sort.first {
                        return (temp[sort.last!], temp[sort.first!], synced)
                    } else {
                        return (temp[sort.last!], nil, synced)
                    }
                } else if synced.count > 0 {
                    return (synced[0], nil, synced)
                }
            }
        }
        return (nil, nil, [])
    }

    func placeWeatherForDay(_ id:Int32, day:Int, mongth:Int, year:Int) -> (Double?, Double?) {
        var components = DateComponents()
        components.day = day
        if mongth > 0 {
            components.month = mongth
        }
        components.year = year
        if let date = Calendar.current.date(from: components), let end = date.endOfDay {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Weather")
            let pred1 = NSPredicate(format: "placeID == %d", id)
            let pred2 = NSPredicate(format: "forecast == NO")
            let pred3 = NSPredicate(format: "date >= %lf", date.startOfDay.timeIntervalSince1970)
            let pred4 = NSPredicate(format: "date < %lf", end.timeIntervalSince1970)
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [pred1, pred2, pred3, pred4])
            let sort = NSSortDescriptor(key: "date", ascending: true)
            fetchRequest.sortDescriptors = [sort]
            if let all = try? managedObjectContext.fetch(fetchRequest) as! [Weather] {
                if all.count > 1 {
                    var temp:[Double] = []
                    for w in all {
                        temp.append(w.temperature)
                    }
                    let sort = temp.sorted()
                    if sort.last != sort.first {
                        return (sort.last, sort.first)
                    } else {
                        return (sort.last, nil)
                    }
                } else if all.count > 0 {
                    return (all[0].temperature, nil)
                }
            }
        }
        return (nil, nil)
    }
}
