//
//  Place+CoreDataClass.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 05.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation

public class Place: NSManagedObject {

    func distance(from: CLLocationCoordinate2D) -> CLLocationDistance {
        let from = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let to = CLLocation(latitude: latitude, longitude: longitude)
        return from.distance(from: to)
    }
    
    var coordinate:CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}
