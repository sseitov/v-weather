//
//  SelectedPlace.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 08.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import Foundation
import CoreLocation
import SVProgressHUD

class SelectedPlace: NSObject {
    var id:Int32 = 0
    var name:String?
    var latitude:Double = 0
    var longitude:Double = 0
    
    var coordinate:CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    class func fromData(_ data:Any) -> SelectedPlace? {
        if let placeData = data as? [String:Any] {
            let place = SelectedPlace()
            if let id = placeData["id"] as? Int32 {
                place.id = id
            } else {
                place.id = 0
            }
            place.name = placeData["name"] as? String
            if let latitude = placeData["latitude"] as? Double {
                place.latitude = latitude
            } else {
                place.latitude = 0
            }
            if let longitude = placeData["longitude"] as? Double {
                place.longitude = longitude
            } else {
                place.longitude = 0
            }
            return place
        } else {
            return nil
        }
    }
}

func addMyPlace(_ place:Place) {
    var places = UserDefaults.standard.object(forKey: "places") as? [Any]
    let data:[String:Any] = ["id" : place.id, "name" : place.name!, "latitude" : place.latitude, "longitude" : place.longitude]
    if places != nil {
        places!.append(data)
    } else {
        places = [data]
    }
    let index:Int = places!.count - 1
    UserDefaults.standard.set(places!, forKey: "places")
    UserDefaults.standard.set(index, forKey: "currentPlace")
    UserDefaults.standard.synchronize()
}
/*
func isInMyPlaces(_ id:Int) -> Bool {
    if let places = UserDefaults.standard.object(forKey: "places") as? [Any] {
        for item in places {
            if let place = item as? [String:Any], let placeId = place["id"] as? Int {
                if placeId == id {
                    return true
                }
            }
        }
        return false
    } else {
        return false
    }
}
*/
func myPlacesID() -> [Int64] {
    var ids:[Int64] = []
    for item in myPlaces() {
        if let place = item as? [String:Any], let placeId = place["id"] as? Int {
            ids.append(Int64(placeId))
        }
    }
    return ids
}

func myPlaces() -> [Any] {
    if let places = UserDefaults.standard.object(forKey: "places") as? [Any] {
        return places
    } else {
        return []
    }
}

func myPlaceIndex() -> Int {
    if let index = UserDefaults.standard.object(forKey: "currentPlace") as? Int {
        return index
    } else {
        return -1
    }
}

func setMyPlaceIndex(_ index:Int) {
    UserDefaults.standard.set(index, forKey: "currentPlace")
    UserDefaults.standard.synchronize()
}

func myPlaceAtIndex(_ index:Int) -> SelectedPlace? {
    if myPlaces().count <= index {
        return nil
    }
    return SelectedPlace.fromData(myPlaces()[index])
}

func deleteCurrentPlace() {
    var places = UserDefaults.standard.object(forKey: "places") as? [Any]
    var index = myPlaceIndex()
    if places == nil || places!.count <= index {
        return
    }
    let data = places![index]
    if let selected = SelectedPlace.fromData(data), let place = Model.shared.getPlace(selected.id) {
        let values = Model.shared.placeWeatherData(place.id)
        for value in values {
            Model.shared.managedObjectContext.delete(value)
        }
        if place.id < 0 {
            Model.shared.managedObjectContext.delete(place)
        }
        Model.shared.saveContext()
    }
    places?.remove(at: index)
    index -= 1
    if index < 0 && places!.count > 0 {
        index = 0
    }
    UserDefaults.standard.set(places!, forKey: "places")
    UserDefaults.standard.set(index, forKey: "currentPlace")
    UserDefaults.standard.synchronize()
}
