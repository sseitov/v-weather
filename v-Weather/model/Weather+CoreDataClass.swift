//
//  Weather+CoreDataClass.swift
//  v-Weather
//
//  Created by Сергей Сейтов on 08.05.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import UIKit
import CoreData

public class Weather: NSManagedObject {
    
    var synced:Bool {
        return recordName != nil
    }

    func isActual() -> Bool {
        let interval = Date().timeIntervalSince(Date(timeIntervalSince1970: date))
        return (interval < 60*3) // inverval from last date less then 3 min
    }
}
