//
//  Weather+CoreDataProperties.swift
//  v-Weather
//
//  Created by Sergey Seitov on 06.08.2018.
//  Copyright © 2018 V-Channel. All rights reserved.
//
//

import Foundation
import CoreData


extension Weather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Weather> {
        return NSFetchRequest<Weather>(entityName: "Weather")
    }

    @NSManaged public var date: Double
    @NSManaged public var forecast: Bool
    @NSManaged public var humidity: Double
    @NSManaged public var placeID: Int32
    @NSManaged public var pressure: Double
    @NSManaged public var stateDescription: String?
    @NSManaged public var stateIcon: String?
    @NSManaged public var temperature: Double
    @NSManaged public var windDirection: Double
    @NSManaged public var windSpeed: Double
    @NSManaged public var recordName: String?

}
