//
//  SyncManager.swift
//  Glucograph
//
//  Created by Sergey Seitov on 18.07.17.
//  Copyright © 2017 V-Channel. All rights reserved.
//

import Foundation
import CloudKit
import CoreData

class SyncManager: NSObject {
    
    static let shared = SyncManager()
    
    private var cloudDB: CKDatabase?
    private var internetReachability:Reachability?
    private var networkStatus:NetworkStatus = NotReachable
    
    private override init() {
        super.init()
        
        let container = CKContainer.default()
        cloudDB = container.privateCloudDatabase
        
        internetReachability = Reachability.forInternetConnection()
        if internetReachability != nil {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(self.reachabilityChanged(_:)),
                                                   name: NSNotification.Name.reachabilityChanged,
                                                   object: nil)
            networkStatus = internetReachability!.currentReachabilityStatus()
            internetReachability!.startNotifier()
        }
    }
    
    // MARK: - Reachability
    
    private func iCloudAvailable() -> Bool {
        return FileManager.default.ubiquityIdentityToken != nil
    }
    
    func networkAvailable() -> Bool {
        return networkStatus != NotReachable
    }
    
    @objc  func reachabilityChanged(_ notify:Notification) {
        if let currentReachability = notify.object as? Reachability {
            networkStatus = currentReachability.currentReachabilityStatus()
        }
    }
    
    // MARK: - Weather cloud
    
    func refreshPlaceWeather(_ complete:@escaping(Bool) -> ()) {
        if !iCloudAvailable() {
            Alert.message(title: "attention".localized, message: "signIn".localized, okHandler: {
                complete(false)
            })
            return
        } else if networkAvailable() {
            getSynced {
                complete(true)
            }
        } else {
            Alert.message(title: "error".localized, message: "networkError".localized, okHandler: {
                complete(false)
            })
        }
    }
    
    private func putUnsynced(_ complete:@escaping(Int)->()) {
        
        func putCloudWeather(_ weather:Weather, complete:@escaping() -> ()) {
            if networkAvailable() {
                let record = CKRecord(recordType: "Weather")
                record.setValue(Int(weather.placeID), forKey: "placeID")
                record.setValue(weather.date, forKey: "date")
                record.setValue(weather.humidity, forKey: "humidity")
                record.setValue(weather.pressure, forKey: "pressure")
                record.setValue(weather.temperature, forKey: "temperature")
                record.setValue(weather.windDirection, forKey: "windDirection")
                record.setValue(weather.windSpeed, forKey: "windSpeed")
                if weather.stateDescription != nil {
                    record.setValue(weather.stateDescription!, forKey: "stateDescription")
                }
                if weather.stateIcon != nil {
                    record.setValue(weather.stateIcon!, forKey: "stateIcon")
                }
                cloudDB!.save(record, completionHandler: { _, error in
                    DispatchQueue.main.async {
                        if error != nil {
                            print(error!.localizedDescription)
                        } else {
                            weather.recordName = record.recordID.recordName
                            Model.shared.saveContext()
                        }
                        complete()
                    }
                })
            } else {
                complete()
            }
        }

        if networkAvailable() {
            let unsynced = Model.shared.unsyncedWeather()
            if unsynced.count > 0 {
                let next = NSCondition()
                DispatchQueue.global().async {
                    var current = 0
                    for item in unsynced {
                        putCloudWeather(item, complete: {
                            current += 1
                            print("put \(current) from \(unsynced.count)")
                            next.lock()
                            next.signal()
                            next.unlock()
                        })
                        next.lock()
                        next.wait()
                        next.unlock()
                    }
                    complete(unsynced.count)
                }
            } else {
                complete(0)
            }
        } else {
            complete(0)
        }
    }
    
    typealias FetchedBlock = (CKRecord) -> Void

    private func getSynced(_ complete:@escaping()->()) {
        
        func addCloudWeather(_ record:CKRecord) {
            if let date = record.value(forKey: "date") as? Double, let placeID = record.value(forKey: "placeID") as? Int64 {
                let weather = Model.shared.createWeather(date: date, place: Int32(placeID), isForecast: false)
                if let humidity = record.value(forKey: "humidity") as? Double {
                    weather.humidity = humidity
                }
                if let pressure = record.value(forKey: "pressure") as? Double {
                    weather.pressure = pressure
                }
                if let temperature = record.value(forKey: "temperature") as? Double {
                    weather.temperature = temperature
                }
                if let windDirection = record.value(forKey: "windDirection") as? Double {
                    weather.windDirection = windDirection
                }
                if let windSpeed = record.value(forKey: "windSpeed") as? Double {
                    weather.windSpeed = windSpeed
                }
                weather.stateDescription = record.value(forKey: "stateDescription") as? String
                weather.stateIcon = record.value(forKey: "stateIcon") as? String
                weather.recordName = record.recordID.recordName
                Model.shared.saveContext()
            }
        }

        let predicate = NSPredicate(format: "placeID IN %@", myPlacesID())
        let query = CKQuery(recordType: "Weather", predicate: predicate)
        var queryOperation = CKQueryOperation(query: query)
        
        let fetchedBlock:FetchedBlock = { (record) in
            DispatchQueue.main.async {
                if Model.shared.getWeather(record.recordID.recordName) == nil {
                    print("add \(record.recordID.recordName)")
                    addCloudWeather(record)
                }
            }
        }
        
        queryOperation.recordFetchedBlock = fetchedBlock
        queryOperation.queryCompletionBlock = { (cursor : CKQueryCursor?, error : Error?) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                if cursor != nil {
                    print("continue...")
                    let nextOperation = CKQueryOperation(cursor: cursor!)
                    nextOperation.recordFetchedBlock = fetchedBlock
                    nextOperation.queryCompletionBlock = queryOperation.queryCompletionBlock
                    queryOperation = nextOperation
                    self.cloudDB?.add(queryOperation)
                } else {
                    DispatchQueue.main.async {
                        complete()
                    }
                }
            }
        }
        
        cloudDB?.add(queryOperation)
    }
    
    private func clearLocal() -> NSArray {
        
        func deleteLocalForDate(_ date:Date, place:Int) -> [String] {
            let maxMin = Model.shared.maxMinWeatherForDay(Int32(place), date: date)
            let all = maxMin.2
            
            var deleted:[String] = []
            var notDelete:[String] = []
            
            if maxMin.0 != nil {
                notDelete.append(maxMin.0!.recordName!)
            }
            if maxMin.1 != nil {
                notDelete.append(maxMin.1!.recordName!)
            }
            
            // clear local DB
            for weather in all {
                if !notDelete.contains(weather.recordName!) {
                    deleted.append(weather.recordName!)
                    Model.shared.deleteWeather(weather)
                }
            }
            
            return deleted
        }

        let deleted = NSMutableArray()
        if let firstDate = Model.shared.firstDate() {
            for item in myPlaces() {
                if let place = item as? [String:Any], let placeId = place["id"] as? Int, let name = place["name"] as? String {
                    print("========= check \(placeId) \(name)")
                    var date = firstDate
                    while true {
                        let result = deleteLocalForDate(date, place: placeId)
                        deleted.addObjects(from: result)
                        if let nextDate = date.nextDay {
                            date = nextDate
                        } else {
                            break
                        }
                    }
                }
            }
        }
        
        return deleted
    }
    
    private func clearCloud(_ records:NSArray, complete:@escaping()->()) {
        let nextRecord = NSCondition()
        
        DispatchQueue.global().async {
            for (_, value) in records.enumerated() {
                if let recordName = value as? String {
                    let recordID = CKRecordID(recordName: recordName)
                    self.cloudDB?.delete(withRecordID: recordID, completionHandler: { _, error in
                        if error != nil {
                            print(error!.localizedDescription)
                        }
                        DispatchQueue.main.async {
                            nextRecord.lock()
                            nextRecord.signal()
                            print("delete completed")
                            nextRecord.unlock()
                        }
                    })
                    nextRecord.lock()
                    nextRecord.wait()
                    nextRecord.unlock()
                } else {
                    print("skip")
                }
            }
            DispatchQueue.main.async {
                complete()
            }
        }
    }
    
    func refreshCloud(_ complete:@escaping(String?)->()) {
        if !iCloudAvailable() {
            complete("signIn".localized)
        } else if !networkAvailable() {
            complete("networkError".localized)
        } else {
            let beforeAll = Model.shared.allWeatherCount()
            let beforeSynced = Model.shared.allSyncedWeatherCount()
            print("all \(beforeAll), synced \(beforeSynced)")
            getSynced({
                let afterAll = Model.shared.allWeatherCount()
                let afterSynced = Model.shared.allSyncedWeatherCount()
                print("after sync all \(afterAll), synced \(afterSynced)")
                let cleared = self.clearLocal()
                let afterClear = Model.shared.allSyncedWeatherCount()
                print("cleared local \(cleared.count), rest \(afterClear)")
                self.clearCloud(cleared, complete: {
                    self.putUnsynced({ count in
                        DispatchQueue.main.async {
                            print("synced \(count)")
                            complete(nil)
                        }
                    })
                })
            })
        }
    }

}
